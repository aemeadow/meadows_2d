﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerMovement : MonoBehaviour
{
    Rigidbody2D rB2D;

    public float runSpeed;
    public float jumpForce;

    public SpriteRenderer spriteRenderer;

    public Animator animator;
    public Text countText;
    private int count;

    // Start is called before the first frame update
    void Start()
    {
        //Fill rigidbody2D refrence to our rigidbody2d
        rB2D = GetComponent<Rigidbody2D>();
        
        count = 0;

        SetCountText ();
    }

    // Update is called once per frame
    private void Update()
    {
        //If the player presses the jump button
        if (Input.GetButtonDown("Jump"))
        {
            //Do a box cast of a very thin box our width directly below us
            int levelMask = LayerMask.GetMask("Level");

            //parameters
            if (Physics2D.BoxCast(transform.position, new Vector2(1f, .1f), 0f, Vector2.down, .01f, levelMask))
            {
                Jump();
            }
        }
    }

    void FixedUpdate()
    {
        //run movement 
        float horizontalInput = Input.GetAxis("Horizontal");
    

        //set velocity 
        rB2D.velocity = new Vector2(horizontalInput * runSpeed * Time.deltaTime, rB2D.velocity.y);

        if (rB2D.velocity.x > 0)
            spriteRenderer.flipX = false;
        else
        if (rB2D.velocity.x < 0)
            spriteRenderer.flipX = true;

        if (Mathf.Abs (horizontalInput) > 0f)
            animator.SetBool("IsRunning", true);
        else 
            animator.SetBool("IsRunning", false);

        if (Input.GetButtonDown("Jump"))
            animator.SetBool("IsJumping", true);
        else
            animator.SetBool("IsJumping", false);
    
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.CompareTag("Gems"))
        {
            other.gameObject.SetActive(false);
            count = count + 1;
            SetCountText ();
        }
    }

    void SetCountText()
    {
        countText.text = "Count: " + count.ToString ();
    }
    void Jump()
    {
        //set velocity 
        rB2D.velocity = new Vector2 (rB2D.velocity.x, jumpForce);
    }
}
